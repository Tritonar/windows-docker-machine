# [Windows Docker Machine](https://github.com/StefanScherer/windows-docker-machine)
Vm con Vagrant sin interface. Se creará los certificados TLS y un `context` de Docker  `2022-box` o `2019-box`.

```
$ git clone https://github.com/StefanScherer/windows-docker-machine
$ cd windows-docker-machine
$ vagrant up --provider virtualbox 2019-box
```

- Nota: Se pude para utilizar Windows Server 2022, reemplazar por 2022-box.

```
docker context ls   

NAME        DESCRIPTION                               DOCKER ENDPOINT               KUBERNETES ENDPOINT                   ORCHESTRATOR
2019-box    2019-box windows-docker-machine           tcp://192.168.59.51:2376                                            
default *   Current DOCKER_HOST based configuration   unix:///var/run/docker.sock   https://192.168.58.2:8443 (default)   swarm
```
# Cambiar a contenedores de Windows

```
$ docker context use 2019-box
```
- Verificar use un `windows-docker-machine`
```
docker context ls
2019-box *   2019-box windows-docker-machine           tcp://192.168.59.51:2376                                            
default      Current DOCKER_HOST based configuration   unix:///var/run/docker.sock   https://192.168.58.2:8443 (default)   swarm
```

# Ejemplos:

- hello-world
```
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
35ab4104a4d9: Pull complete 
732a68771cf1: Pull complete 
1491898410a2: Pull complete 
Digest: sha256:faa03e786c97f07ef34423fccceeec2398ec8a5759259f94d99078f264e9d7af
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (windows-amd64, nanoserver-1809)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run a Windows Server container with:
 PS C:\> docker run -it mcr.microsoft.com/windows/servercore:1809 powershell

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```
- aspnetapp
```
docker run -it --rm -p 8000:80 --name aspnetcore_sample mcr.microsoft.com/dotnet/samples:aspnetapp

warn: Microsoft.AspNetCore.DataProtection.Repositories.FileSystemXmlRepository[60]
      Storing keys in a directory 'C:\Users\ContainerUser\AppData\Local\ASP.NET\DataProtection-Keys' that may not be persisted outside of the container. Protected data will be unavailable when container i
s destroyed.
info: Microsoft.Hosting.Lifetime[14]
      Now listening on: http://[::]:80
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down. 
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Production
info: Microsoft.Hosting.Lifetime[0]
      Content root path: C:\app
warn: Microsoft.AspNetCore.HttpsPolicy.HttpsRedirectionMiddleware[3] 
      Failed to determine the https port for redirect.
```
![img](img/app_Net.png)